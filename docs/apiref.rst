=============
API Reference
=============

API reference for ``flufl.enum``:


.. autoclass:: flufl.enum.Enum
   :members:

.. autoclass:: flufl.enum.IntEnum
   :members:

.. autoclass:: flufl.enum.EnumValue
   :members:
